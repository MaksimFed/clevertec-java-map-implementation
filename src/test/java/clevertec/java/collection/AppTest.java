package clevertec.java.collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testIsEmpty(){
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(0,0);
        assertTrue(!(map.size() == 0));
    }

    @Test
    public void  testPut(){
        HashMap<Integer,Integer> map = new HashMap<>();
        Integer actual = map.put(0,1);
        Integer expected = null;
        assertEquals(expected,actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void  testNegativeCapacity(){
        HashMap<Integer,Integer> map = new HashMap<>(-10);
    }

    @Test
    public void testGet(){
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(1,10);
        map.put(2,20);
        Integer actual = map.get(1);
        Integer expected = 10;
        assertEquals(expected,actual);
    }

    @Test
    public void testContainsKey(){
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(0,0);
        map.put(1,1);
        map.put(2,2);
        boolean actual = map.containsKey(1);
        boolean expected = true;
        assertEquals(expected,actual);
    }

    @Test
    public void testContainsValue(){
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(0,0);
        map.put(1,1);
        map.put(2,2);
        boolean actual = map.containsValue(3);
        boolean expected = false;
        assertEquals(expected,actual);
    }
}

package clevertec.java.collection;

import java.util.Arrays;
import java.util.Collection;

public class HashMap<K extends Object, V extends Object> implements IMap<K, V> {

    private Entry<K,V> [] table;
    private int size;
    private static final int DEFAULT_CAPACITY = 16;
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private int threshold;
    private final float loadFactor;

    public HashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " +
                    initialCapacity);
        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " +
                    loadFactor);

        // Find a power of 2 >= initialCapacity
        int capacity = 1;
        while (capacity < initialCapacity)
            capacity <<= 1;

        this.loadFactor = loadFactor;
        threshold = (int)(capacity * loadFactor);
        table = new Entry[capacity];

    }

    public HashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    public HashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int)(DEFAULT_CAPACITY * DEFAULT_LOAD_FACTOR);
        table = new Entry[DEFAULT_CAPACITY];
    }

    private int hash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    private int indexFor(int h, int length) {
        return h & (length-1);
    }

    @Override
    public void clear() {
        for (int i = 0; i < table.length; i++){
            table[i] = null;
        }
        size = 0;
    }

    @Override
    public V get(Object key) {
        if (key == null){
            return getForNullKey();
        }
        int hash = hash(key.hashCode());
        for (Entry<K,V> e = table[indexFor(hash,table.length)]; e != null; e = e.next){
            if (e.hash == hash && (e.key == key || e.key.equals(key))){
                return e.value;
            }
        }
        return null;
    }

    private V getForNullKey() {
        for (Entry<K,V> e = table[0]; e != null; e = e.next){
            if (e.key == null){
                return e.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        if (key == null){
            return putForNullKey(value);
        }
        int hash = hash(key.hashCode());
        for (Entry<K,V> e = table[indexFor(hash,table.length)]; e != null; e = e.next){
            //check if such key already exists and replace value
            if (e.key == key || e.key.equals(key)){
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }

        //add new entry
        addEntry(key,value,hash,indexFor(hash,table.length));
        return null;
    }

    private V putForNullKey(V value){
        for (Entry<K,V> e = table[0]; e != null; e = e.next){
            if (e.key == null){
                //replace existing value
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        //add new Entry
        addEntry(null,value,0,0);
        return null;
    }

    private void addEntry(K key, V value, int hash, int i) {
        Entry<K,V> newEntry = new Entry<>(key,value,table[i],hash);
        table[i] = newEntry;
        if (size++ >= threshold){
            resize(2 * table.length);
        }
    }

    private void resize(int newCapacity){
        if (table.length == MAXIMUM_CAPACITY)
        {
            threshold = Integer.MAX_VALUE;
            return;
        }

        Entry<K,V> [] newTable = new Entry[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int)(newCapacity * loadFactor);
    }

    private void transfer(Entry<K,V> [] newTable){
        for (int i = 0; i < table.length; i++){
            for (Entry<K,V> e = table[i]; e != null;){
                int index = indexFor(e.hash,newTable.length);
                Entry<K,V> next = e.next;
                e.next = newTable[index];
                newTable[index] = e;
                e = next;

            }

        }
    }

    @Override
    public V remove(Object key) {
        int hash = (key == null) ? 0 : hash(key.hashCode());
        int i = indexFor(hash,table.length);
        Entry<K,V> prev = table[i];
        Entry<K,V> e = prev;

        while (e != null) {
            Entry<K,V> next = e.next;
            Object k;
            if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k)))) {
                size--;
                if (prev == e)
                    table[i] = next;
                else
                    prev.next = next;
                return e.value;
            }
            prev = e;
            e = next;
        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        int hash = (key == null) ? 0 : hash(key.hashCode());
        int i = indexFor(hash,table.length);
        for (Entry<K,V> e = table[i]; e != null; e = e.next) {
            if (e.hash == hash && ( e.key == key || e.key.equals(key))){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        if (value == null){
            return containsNullValue();
        }
        for (int i = 0 ; i < table.length; i++){
            for (Entry<K,V> e = table[i] ; e != null; e = e.next){
                if (e.value.equals(value)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean containsNullValue(){
        for (int i = 0 ; i < table.length; i++){
            for (Entry<K,V> e = table[i] ; e != null; e = e.next){
                if (e.value == null){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Collection<V> values() {
        //is that appropriate in our case?
        V [] values = (V [])new Object[size];
        int i = 0;
        for (Entry<K,V> it : table){
            for (Entry<K,V>  e = it; e != null; e = e.next){
                values[i++] = e.value;
            }
        }
        return Arrays.asList(values);

    }

    static class Entry<K,V>{
        final K key;
        V value;
        Entry<K,V> next;
        int hash;

        public Entry(K key, V value, Entry<K, V> next, int hash) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.hash = hash;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry<?, ?> entry = (Entry<?, ?>) o;

            if (hash != entry.hash) return false;
            if (key != null ? !key.equals(entry.key) : entry.key != null) return false;
            if (value != null ? !value.equals(entry.value) : entry.value != null) return false;
            return next != null ? next.equals(entry.next) : entry.next == null;
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }
}
